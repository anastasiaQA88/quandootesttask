# Tools used:  
Java (Intellij IDEA)  
Maven  
Allure (reports)  
TestNG  

# To start tests you should have
1) Installed JDK and all Java environment variables  
https://www.oracle.com/java/technologies/javase-downloads.html

2) Installed IDEA, Community version   
https://www.jetbrains.com/idea/download/

3) Clone project to your idea space and then start mvn dependency:resolve command   
to resolve all pom.xml dependencies

# To run tests with reports (3 types of reports are available)
1) To run tests use command mvn test, after tests will be finished you can run allure:serve   
command to see Allure tool generated test report with all times and steps to cases

2) To run test and to see just mvn report(html or xml format) run surefire-report:report   
command, and after all will be finished you will see HTML and XML report, named AllTests in project folder  
target-surefire-reports-All Tests  

