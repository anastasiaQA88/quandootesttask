import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.basePage;
import pages.mainPage;

import java.util.ArrayList;

import static helpers.testData.*;
import static pages.basePage.*;
import static pages.mainPage.*;


public class commonTests {

    public WebDriver driver;
    public pages.basePage basePage;
    //public Properties properties;
    private pages.mainPage mainPage;

    @BeforeMethod
    public void setup() {

        basePage = new basePage();
        driver = basePage.initialize_driver();
        mainPage = new mainPage(driver);

    }

    @Test(description = "Login Success", priority = 0)
    public void testLoginSuccess() throws Exception {

        openPage(driver, loginUrl);
        enterField(usernameField, usernameValid);
        enterField(passwordField, passwordValid);
        clickButton(driver, loginButton);
        Assert.assertTrue(elementDisplayed(driver, goodLoginMessage), "No message success login is displayed");

    }

    @Test(description = "Login failure 1", priority = 1)
    public void testLoginFailure1() throws Exception {

        openPage(driver, loginUrl);
        enterField(usernameField, usernameInvalid);
        clickButton(driver, loginButton);
        Assert.assertTrue(elementDisplayed(driver, badUsernameMessage), "No bad username message is displayed");

    }

    @Test(description = "Login failure 2", priority = 2)
    public void testLoginFailure2() throws Exception {

        openPage(driver, loginUrl);
        enterField(usernameField, usernameValid);
        enterField(passwordField, passwordInvalid);
        clickButton(driver, loginButton);
        Assert.assertTrue(elementDisplayed(driver, badPasswordMessage), "No bad password message is displayed");

    }

    @Test(description = "Hovers", priority = 3)
    public void testCheckHovers() throws Exception {

        openPage(driver, hoversUrl);
        hoverOnElement(driver, userAvatar1);
        Assert.assertTrue(elementDisplayed(driver, userName1Hover), "No username 1 is displayed after hover");
        hoverOnElement(driver, userAvatar2);
        Assert.assertTrue(elementDisplayed(driver, userName2Hover), "No username 2 is displayed after hover");
        hoverOnElement(driver, userAvatar3);
        Assert.assertTrue(elementDisplayed(driver, userName3Hover), "No username 3 is displayed after hover");

    }

    @Test(description = "sortable data table", priority = 4)
    public void testSortableTableData() throws Exception {

        openPage(driver, tablesUrl);
        ArrayList<String> list = null;

        list = addArrayValuesToList();
        if (checkArraySortedAsc(list)) {
                System.out.println("Last names are sorted ASC");
                Assert.assertTrue(checkArraySortedAsc(list));
                clickButton(driver, table2LastNameColumnHeader);
                list = addArrayValuesToList();
                    if (checkArraySortedAsc(list)) System.out.println("Last names are sorted ASC");
                    if (checkArraySortedDesc(list)) System.out.println("Last names are sorted DESC");
                    else System.out.println("List is not sorted");
        }
        if (checkArraySortedDesc(list)) {
                System.out.println("Last names are sorted DESC");
                Assert.assertTrue(checkArraySortedDesc(list));
                clickButton(driver, table2LastNameColumnHeader);
                list = addArrayValuesToList();
                    if (checkArraySortedAsc(list)) System.out.println("Last names are sorted ASC");
                    if (checkArraySortedDesc(list)) System.out.println("Last names are sorted DESC");
                    else System.out.println("List is not sorted");
        }
            else {
                System.out.println("List is not sorted");
                clickButton(driver, table2LastNameColumnHeader);
                list = addArrayValuesToList();
                    if (checkArraySortedAsc(list)) {
                        System.out.println("Last names are sorted ASC");
                        Assert.assertTrue(checkArraySortedAsc(list));
                        clickButton(driver, table2LastNameColumnHeader);
                        list = addArrayValuesToList();
                            if (checkArraySortedAsc(list)) System.out.println("Last names are sorted ASC");
                            if (checkArraySortedDesc(list)) System.out.println("Last names are sorted DESC");
                            else System.out.println("List is not sorted");
                        }

                   else {
                        System.out.println("Last names are sorted DESC");
                        Assert.assertTrue(checkArraySortedDesc(list));
                        clickButton(driver, table2LastNameColumnHeader);
                        list = addArrayValuesToList();
                            if (checkArraySortedAsc(list)) System.out.println("Last names are sorted ASC");
                            if (checkArraySortedDesc(list)) System.out.println("Last names are sorted DESC");
                            else System.out.println("List is not sorted");
                    }


        }

    }


    @AfterMethod
    public void tearDown(ITestResult result) {
        makeScreenOnTestFail(result);
        if (driver != null) {
            driver.quit();
        }
    }


}
