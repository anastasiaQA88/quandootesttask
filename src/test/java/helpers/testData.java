package helpers;

public class testData {

    private static String url = "http://the-internet.herokuapp.com/";
    public static String mainUrl = url;
    public static String loginUrl = url + "login";
    public static String hoversUrl = url + "hovers";
    public static String tablesUrl = url + "tables";

    public static String usernameValid = "tomsmith";
    public static String passwordValid = "SuperSecretPassword!";
    public static String usernameInvalid = "blablabla";
    public static String passwordInvalid = "1234abc";


}
