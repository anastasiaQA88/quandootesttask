package pages;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static helpers.testAllureListener.getTestMethodName;
import static helpers.testAllureListener.saveTextLog;
import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;
import static io.github.bonigarcia.wdm.DriverManagerType.FIREFOX;


public class basePage {

    public static ThreadLocal<WebDriver> tdriver = new ThreadLocal<WebDriver>();
    public WebDriver driver;


    public static synchronized WebDriver getDriver() {
        return tdriver.get();
    }

    @Step("Open URL")
    public static void openPage(WebDriver driver, String URL) {
        driver.get(URL);
        String currentURL = driver.getCurrentUrl();
        waitUntilUrlToBe(driver, URL);
        Assert.assertTrue(currentURL.equals(URL), "Не удалось открыть заданный URL");

    }

    @Step("Check that proper URL opened")
    public static boolean checkPageOpened(WebDriver driver, String URL) {

        String currentURL = driver.getCurrentUrl();
        System.out.println(currentURL);
        return currentURL.equals(URL);
    }

    @Step("Click button, JS")
    public static void clickButton(WebDriver driver, WebElement element) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);

    }

    @Step("Hover element")
    public static void hoverOnElement(WebDriver driver, WebElement element) {
        Actions act = new Actions(driver);
        act.moveToElement(element).build().perform();
    }


    @Step("Check if element is displayed")
    public static boolean elementDisplayed(WebDriver driver, WebElement element) {

        waitElementToBeClickable(driver, element);
        return element.isDisplayed();
    }


    @Step("Clear text field")
    public static void clearField(WebDriver driver, WebElement element) {

        element.clear();
        element.sendKeys("");

    }

    @Step("Wait until needed url will be opened")
    public static void waitUntilUrlToBe(WebDriver driver, String url) {

        WebDriverWait wait = new WebDriverWait(driver, 75);
        wait.until(ExpectedConditions.urlContains(url));
    }

    @Step("Wait until element is clickable")
    public static void waitElementToBeClickable(WebDriver driver, WebElement element) {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(element));

    }


    //Image attachments for Allure
    @Step("Take screenshoot for allure")
    @Attachment(value = "Page screenshot", type = "image/png")
    public static byte[] saveScreenshotPNG(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }


    @Step("close driver + screen on fail")
    public static void makeScreenOnTestFail(ITestResult result) {

        if (result.getStatus() == ITestResult.FAILURE) {

            WebDriver driver = basePage.getDriver();
            // Allure ScreenShotRobot and SaveTestLog
            if (driver instanceof WebDriver) {
                System.out.println("Screenshot captured for test case:" + getTestMethodName(result));
                saveScreenshotPNG(driver);
            }
            // Save a log on allure.
            saveTextLog(getTestMethodName(result) + " failed and screenshot taken!");

        }

    }

    @Step("Create webdriver")
    public WebDriver initialize_driver() {

        WebDriverManager.getInstance(CHROME).config().setChromeDriverVersion("79");
        WebDriverManager.getInstance(CHROME).setup();
        WebDriverManager.getInstance(FIREFOX).setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        options.addArguments("enable-automation");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-infobars");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-browser-side-navigation");
        options.addArguments("--disable-gpu");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        tdriver.set(driver);
        return getDriver();
    }


    @Step("Take screen")
    public String getScreenshot() {
        File src = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
        String path = System.getProperty("user.dir") + "/screenshots/" + System.currentTimeMillis() + ".png";
        File destination = new File(path);
        try {
            FileUtils.copyFile(src, destination);
        } catch (IOException e) {
            System.out.println("Capture Failed " + e.getMessage());
        }
        return path;
    }



}







