package pages;

import com.google.common.collect.Ordering;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class mainPage extends basePage {

    @FindBy(id = "username")
    public static WebElement usernameField;

    @FindBy(id = "password")
    public static WebElement passwordField;

    @FindBy(xpath = "//i[contains(.,' Login')]")
    public static WebElement loginButton;

    @FindBy(xpath = "//*[contains(text(),'You logged into a secure area')]")
    public static WebElement goodLoginMessage;

    @FindBy(xpath = "//*[contains(text(),'Your username is invalid')]")
    public static WebElement badUsernameMessage;

    @FindBy(xpath = "//*[contains(text(),'Your password is invalid')]")
    public static WebElement badPasswordMessage;

    @FindBy(xpath = "//img[@alt='User Avatar']")
    public static WebElement userAvatar1;

    @FindBy(xpath = "//*[contains(text(),'name: user1')]")
    public static WebElement userName1Hover;

    @FindBy(xpath = "(//img[@alt='User Avatar'])[2]")
    public static WebElement userAvatar2;

    @FindBy(xpath = "//*[contains(text(),'name: user2')]")
    public static WebElement userName2Hover;

    @FindBy(xpath = "(//img[@alt='User Avatar'])[3]")
    public static WebElement userAvatar3;

    @FindBy(xpath = "//*[contains(text(),'name: user3')]")
    public static WebElement userName3Hover;

    @FindBy(css = ".header > .last-name")
    public static WebElement table2LastNameColumnHeader;

    @FindBy(id = "table2")
    public static WebElement table2;

    @FindBy(xpath = "//table[@id='table2']/tbody/tr/td[1]")
    public static List<WebElement> tdList;

    //initiate main page with it's elements
    public mainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Step("Enter data in text field")
    public static void enterField(WebElement element, String value) {

        element.clear();
        element.sendKeys("");
        element.sendKeys(value);

    }

    @Step("Check array ASC sorting")
    public static boolean checkArraySortedAsc(ArrayList<String> list){
        return Ordering.natural().isOrdered(list);
    }

    @Step("Check array DESC sorting")
    public static boolean checkArraySortedDesc(ArrayList<String> list){
        return Ordering.natural().reverse().isOrdered(list);

    }

    @Step("Add array values to list")
    public static ArrayList<String> addArrayValuesToList() {

        String strArray[] = new String[tdList.size()];
        ArrayList<String> list = new ArrayList<String>();
        for(int i =0;i<tdList.size();i++)
        {
            System.out.println(tdList.get(i).getText());
            strArray[i]=tdList.get(i).getText();
            list.add(strArray[i]);

        }
        System.out.println(list);
        return list;
    }








}
